package lab02;

import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Main {
	public static void main(String[] args) {
        var scanner = new Scanner(System.in);
 
        while ( true ) {
            System.out.print("Number: ");
            if ( ! scanner.hasNextInt() )
                break;
            int n = scanner.nextInt();
            if ( n < 0 )
                break;
            int s = String.valueOf(n).chars().map(a -> a - '0').sum();
            System.out.println("Sum of digits: " + s);
        } 
 
        scanner.close();
    }
}