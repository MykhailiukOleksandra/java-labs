package lab01;

public class Main {

	private static final int DECENT_NUMBER = 0x15D8C; // 89484 => 0x15D8C
    private static final long TELEPHONE_NUMBER = 380689619347L;
    private static final int LAST_2_NUMBERS = 0b101111; // 47 => 00101111
    private static final int LAST_4_NUMBERS = 022203; // 9347 => 022203
    private static final double SECTION_NUMBER_FIVE = ((10 - 1) / 26) + 1;
    private static final char LETTER = 61 + 13;

    /**
     * Method that counts odd numbers from a given bunch of numbers
     * @return number of odds
     * @param number bunch of numbers from which odd numbers will be counted */
    static int oddCount(int... number) {
        int oddCount = 0;
        for (int j : number) if (j % 2 == 0) oddCount++;
        return oddCount;
    }

    /**
     * Method that counts the number of 1 from a given bunch of numbers
     * @return number of 1
     * @param number bunch of numbers from which 1 will be counted */
    static int onesCount(int... number) {
        int onesCount = 0;
        for (int i = 0; i < number.length; i++) {
            while (number[i] != 0) {
                if (number[i] % 2 != 0) {
                    onesCount++;
                }
                number[i] /= 2;
            }
        }
        return onesCount;
    }

    public static void main(String[] args) {
        int result = oddCount(DECENT_NUMBER, (int) TELEPHONE_NUMBER, LAST_2_NUMBERS, LAST_4_NUMBERS);
        int result1 = onesCount(DECENT_NUMBER, (int) TELEPHONE_NUMBER, LAST_2_NUMBERS, LAST_4_NUMBERS);
        System.out.println(DECENT_NUMBER);
        System.out.println(TELEPHONE_NUMBER);
        System.out.println(LAST_2_NUMBERS);
        System.out.println(LAST_4_NUMBERS);
        System.out.println(SECTION_NUMBER_FIVE);
        System.out.println(LETTER);
        System.out.println("odd:");
        System.out.println(result);
        System.out.println("even:");
        System.out.println(result1);
    }
}